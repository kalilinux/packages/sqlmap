sqlmap (1.0.5-0kali1) kali-dev; urgency=medium

  * New uptream release

 -- Sophie Brun <sophie@freexian.com>  Tue, 03 May 2016 11:45:01 +0200

sqlmap (1.0.4-1) unstable; urgency=medium

  * New upstream release.

 -- Gianfranco Costamagna <locutusofborg@debian.org>  Tue, 05 Apr 2016 11:07:51 +0200

sqlmap (1.0.3-1) unstable; urgency=medium

  * New upstream release.

 -- Gianfranco Costamagna <locutusofborg@debian.org>  Thu, 17 Mar 2016 16:52:59 +0100

sqlmap (1.0+git20160227-0kali1) kali-dev; urgency=medium

  * Synchronize with debian (keep a version number with git to follow kali
    old versions)

 -- Sophie Brun <sophie@freexian.com>  Tue, 01 Mar 2016 09:38:31 +0100

sqlmap (1.0-2) unstable; urgency=medium

  * Bump std-version to 3.9.7, no changes required.
  * Bump watch file version to 4.

 -- Gianfranco Costamagna <locutusofborg@debian.org>  Sat, 27 Feb 2016 16:56:20 +0100

sqlmap (1.0-1) unstable; urgency=medium

  * New upstream stable release.
  * Fix watch file to point to github released tarballs.
  * Update copyright year.

 -- Gianfranco Costamagna <locutusofborg@debian.org>  Sat, 27 Feb 2016 16:28:51 +0100

sqlmap (1.0+git20151218-0kali1) kali-dev; urgency=medium

  * Import new upstream release
  * Update version to be consistent with previous versions in kali
  * Drop helper-script (replaced by symlink in debian)

 -- Sophie Brun <sophie@freexian.com>  Fri, 18 Dec 2015 16:05:24 +0100

sqlmap (0.9.160121-1) unstable; urgency=medium

  * New upstream release, update copyright year.

 -- Gianfranco Costamagna <locutusofborg@debian.org>  Thu, 21 Jan 2016 12:31:39 +0100

sqlmap (0.9.151222-2) unstable; urgency=medium

  * Update manpage (Closes: #809245)
    - thanks Jonathan Wiltshire for the bug report!

 -- Gianfranco Costamagna <locutusofborg@debian.org>  Mon, 28 Dec 2015 17:59:40 +0100

sqlmap (0.9.151222-1) unstable; urgency=medium

  * New upstream release.

 -- Gianfranco Costamagna <locutusofborg@debian.org>  Tue, 22 Dec 2015 12:04:12 +0100

sqlmap (0.9.151117-1) unstable; urgency=medium

  * New upstream release.

 -- Gianfranco Costamagna <locutusofborg@debian.org>  Tue, 17 Nov 2015 10:06:40 +0100

sqlmap (0.9.151029-1) unstable; urgency=medium

  * New upstream release.
  * Update copyright file.

 -- Gianfranco Costamagna <locutusofborg@debian.org>  Thu, 29 Oct 2015 18:14:40 +0100

sqlmap (0.9.151005-1) unstable; urgency=medium

  * New upstream release.

 -- Gianfranco Costamagna <locutusofborg@debian.org>  Mon, 05 Oct 2015 17:26:39 +0200

sqlmap (0.9.150908-1) unstable; urgency=medium

  * New upstream release.
    - fix for sqlmapapi usage (upstream issue: #1394)

 -- Gianfranco Costamagna <locutusofborg@debian.org>  Tue, 08 Sep 2015 12:54:52 +0200

sqlmap (0.9.150813-1) unstable; urgency=medium

  * New upstream release.
  * Update my uid.

 -- Gianfranco Costamagna <locutusofborg@debian.org>  Thu, 13 Aug 2015 16:51:33 +0200

sqlmap (1.0+git20150728-0kali1) kali-dev; urgency=medium

  * New upstream snapshot.
  * Fix watch file to monitor github.

 -- Raphaël Hertzog <hertzog@debian.org>  Tue, 28 Jul 2015 10:58:11 +0200

sqlmap (0.9.150623-2) unstable; urgency=medium

  * Add the example sqlmap.conf in /etc/sqlmap

 -- Gianfranco Costamagna <costamagnagianfranco@yahoo.it>  Thu, 25 Jun 2015 14:09:50 +0200

sqlmap (0.9.150623-1) unstable; urgency=medium

  * New upstream snapshot.
  * Add sqlmapapi.py tool to the build.
    - Thanks to Raffaele Forte for the report.
  * Add sqlmapapi manpage.

 -- Gianfranco Costamagna <costamagnagianfranco@yahoo.it>  Fri, 12 Jun 2015 16:30:59 +0200

sqlmap (0.9.150525-1) unstable; urgency=medium

  * New upstream snapshot.

 -- Gianfranco Costamagna <costamagnagianfranco@yahoo.it>  Mon, 20 Apr 2015 10:14:23 +0200

sqlmap (0.9.150320-1) unstable; urgency=medium

  * New upstream snapshot.

 -- Gianfranco Costamagna <costamagnagianfranco@yahoo.it>  Sun, 15 Mar 2015 01:03:17 +0100

sqlmap (1.0+git20150214-0kali1) kali; urgency=medium

  * Import new version from debian (Closes: 0002160)
  * Update version to be conistent with previous versions in kali 
  * Remove file debian/sqlmap.install (replaces in debian with debian/install)
    and add the helper script installation in debian/install
  * Remove file debian/dirs

 -- Sophie Brun <sophie@freexian.com>  Mon, 16 Mar 2015 08:53:03 +0100

sqlmap (0.9.150214-1) unstable; urgency=medium

  * New upstream release.
  * Remove all lintian-overrides, upstream renamed .so in .so_ and
    .dll to .dll_, so lintian doesn't catch them anymore.
  * Update copyright year.

 -- Gianfranco Costamagna <costamagnagianfranco@yahoo.it>  Sat, 31 Jan 2015 18:25:34 +0100

sqlmap (0.9.141209-1) unstable; urgency=low

  [Luciano Bello]
  * Reintroducing release (Closes: #741544)
  * New upstream version (Closes: #685729, #719693)
  * debian/control:
    - New upstream homepage

  [Gianfranco Costamagna] 
  * Update copyright file.
  * Standards-Version: 3.9.6
  * Fix hypens as minus sign.
  * Quilt 3.0 source package.
  * Create a "tarball" rule target.

 -- Luciano Bello <luciano@debian.org>  Wed, 24 Dec 2014 14:39:36 +0100

sqlmap (1.0+git20140307-0kali1) kali; urgency=low

  * Update package to non-native, switch to dpkg-source 3.0 (quilt) format.

 -- Mati Aharoni <muts@kali.org>  Fri, 07 Mar 2014 15:16:12 -0500

sqlmap (1.0+git20140307) kali; urgency=low

  * Upstream update and UDF binary file fix

 -- Mati Aharoni <muts@kali.org>  Fri, 07 Mar 2014 10:04:14 -0500

sqlmap (1.0+git20140123) kali; urgency=low

  * Upstream import

 -- Emanuele Acri <crossbower@kali.org>  Thu, 23 Jan 2014 16:26:03 +0100

sqlmap (1.0+git20130820) kali; urgency=low

  * Upstream import

 -- Mati Aharoni <muts@kali.org>  Tue, 20 Aug 2013 22:36:56 -0400

sqlmap (1.0+git20130217-1kali0) kali; urgency=low

  * Fixed launch errors

 -- Devon Kearns <dookie@kali.org>  Thu, 28 Feb 2013 11:32:52 -0700

sqlmap (1.0+git20130217) kali; urgency=low

  * Updated from git

 -- Mati Aharoni <muts@kali.org>  Thu, 07 Feb 2013 13:30:54 -0500

sqlmap (0.9~svn2269) kali; urgency=low

  * Version update

 -- Mati Aharoni <muts@kali.org>  Mon, 17 Dec 2012 17:02:20 -0500

sqlmap (0.6.4-1kali0) kali; urgency=low

  * Kali import

 -- Mati Aharoni <muts@kali.org>  Mon, 17 Dec 2012 16:23:20 -0500

sqlmap (0.6.4-1) unstable; urgency=low

  * Initial release (Closes: #497667)

 -- Bernardo Damele A. G. <bernardo.damele@gmail.com>  Tue,  3 Feb 2009 23:30:00 +0000
